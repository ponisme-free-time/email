package th.co.oreopon.email.repository;

import org.springframework.data.repository.CrudRepository;
import th.co.oreopon.email.entity.EmailRegister;

public interface EmailRegisterRepository extends CrudRepository<EmailRegister,String> {
}
