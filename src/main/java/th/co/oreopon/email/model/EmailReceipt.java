package th.co.oreopon.email.model;

import lombok.Data;

@Data
public class EmailReceipt {
    private String userId;
    private String email;
    private String event;
}
