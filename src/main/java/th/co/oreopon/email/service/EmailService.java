package th.co.oreopon.email.service;

import org.springframework.stereotype.Service;
import th.co.oreopon.email.entity.EmailRegister;
import th.co.oreopon.email.model.EmailReceipt;
import th.co.oreopon.email.repository.EmailRegisterRepository;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class EmailService {
    private final EmailRegisterRepository emailRegisterRepository;

    public EmailService(EmailRegisterRepository emailRegisterRepository) {
        this.emailRegisterRepository = emailRegisterRepository;
    }

    public void registerEmail(EmailReceipt emailReceipt){
        String emailId="meail_"+ UUID.randomUUID().toString();
        EmailRegister emailRegister= EmailRegister.builder()
                .emailId(emailId)
                .email(emailReceipt.getEmail())
                .userId(emailReceipt.getUserId())
                .createdDate(LocalDateTime.now())
                .updatedDate(LocalDateTime.now())
                .build();
        emailRegisterRepository.save(emailRegister);


    }}
