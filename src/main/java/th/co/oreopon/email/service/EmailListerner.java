package th.co.oreopon.email.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import th.co.oreopon.email.model.EmailReceipt;

@Component
public class EmailListerner {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailListerner.class);

    private final EmailService emailService;

    public EmailListerner(EmailService emailService) {
        this.emailService = emailService;
    }

    @RabbitListener(queues = {"${rabbitmq.queue.name}"})
    public void consume(String message) throws JsonProcessingException {
        LOGGER.info(String.format("Received message -> %s", message));
        ObjectMapper mapper = new ObjectMapper();
        EmailReceipt emailReceipt = mapper.readValue(message, EmailReceipt.class);
        emailService.registerEmail(emailReceipt);
    }
}
