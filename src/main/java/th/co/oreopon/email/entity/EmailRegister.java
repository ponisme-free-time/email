package th.co.oreopon.email.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@Entity
@Builder
public class EmailRegister {
    @Id
    private String emailId;
    private String userId;
    private String email;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;

    public EmailRegister() {
    }
}
